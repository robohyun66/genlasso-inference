# Exact LAR p value
Gv.pval = function(y,G,v,sigma=1,max=FALSE, threshold=TRUE, tol=1E-10) {
  r = G %*% v
  if(threshold) r[abs(r)<tol]=0
  V = -G %*% (y - v*sum(v*y)/sum(v*v)) / r * sum(v*v)
  Vlo = max(V[r>0])
  Vup = min(V[r<0])
  denom = sigma*sqrt(sum(v*v))
  a = Vlo/denom
  b = Vup/denom
  x = max(min(sum(v*y),Vup),Vlo)/denom
  if (max) return(max_truncnorm(x,a,b))
  return(rob_truncnorm(x,a,b))
}

#Approximates integral_a^b e^{-x^2/2+offset^2/2} dx
# offset is used to make ratios slightly more stable
# defaults to offset=0
# Note that I've left out 1/sqrt(2*pi), you can add it in if you like
approx_int = function(a,b,n=1000,offset=0){
  delta = (b-a)/n #Step size, may want to vary in the future
  x = seq(from=a,to=b,by=delta)
  y = -x^2/2 + offset^2/2 # On the log scale
  m = diff(y)/diff(x) # Line segment slopes
  de = diff(exp(y)) # Difference on original scale
  sum(de/m) #Sum of integrals of line segments (closed form)
}

#Uses approx_int to evaluate int_x^b phi(z)dz / int_a^b phi(z)dz
#Right now offsets everything by e^{-a^2/2} for a little more stability
max_truncnorm = function(x,a,b,n=1000){
  left = approx_int(a,x,n,a)
  right = approx_int(x,b,n,a)
  right/(left+right)
}

rob_truncnorm = function(z,a,b,mean=0,sd=1){
 # return Prob(Z<z | Z in [a,b])
 #"mean" can be a vector
#uses 
# A UNIFORM APPROXIMATION TO THE RIGHT NORMAL TAIL INTEGRAL
# W Bryc
#Applied Mathematics and Computation
#Volume 127, Issues 2–3, 15 April 2002, Pages 365–374
#https://math.uc.edu/~brycw/preprint/z-tail/z-tail.pdf
    
f=function(z){
(z^2+5.575192695*z+12.7743632)/(z^3*sqrt(2*pi)+ 14.38718147*z*z+31.53531977*z+2*12.77436324)
}
#first try standard  formula
a=(a-mean)/sd
b=(b-mean)/sd
z=(z-mean)/sd
term1=1-pnorm(a)
term2=1-pnorm(b)
term3=1-pnorm(z)
out=(term1-term3)/(term1-term2)
o=is.na(out)

#if any are NAs, apply modified method
if(sum(o)>0){
     zz=z[o]
    aa=a[o]
     bb=b[o]
    term1=exp(zz*zz)
    oo=aa>-Inf
   term1[oo]=f(aa[oo])*exp(-(aa[oo]^2-zz[oo]^2)/2)
   term2=0
    ooo=bb<Inf
    term2[ooo]=f(bb[ooo])*exp(-(bb[ooo]^2-zz[ooo]^2)/2)
   out[o]= (term1-f(zz))/ (term1-term2)
}
return(1-out)
}
 

# Exact LAR p values list
lar.pval.list = function(y,out,k,sigma=1) {
  pvals = numeric(k)
  for (i in 1:k) {
    G = out$Gamma[1:out$nk[i],]
    v = out$Gamma[out$nk[i],]
    pvals[i] = Gv.pval(y,G,v,sigma)
  }
  return(pvals)
}

# Spacing LAR p values list
spacing.pval.list = function(y,out,k,sigma=1) {
  pvals = numeric(k)
  for (i in 1:k) {
    v = out$Gamma[out$nk[i],]
    denom = sigma*sqrt(sum(v*v))
    if (i==1) {
      pvals[1] = (1-pnorm(out$lambda[i]/denom))/
        (1-pnorm(out$mp[i]/denom))
    }
    else {
      pvals[i] = (pnorm(out$lambda[i-1]/denom)-pnorm(out$lambda[i]/denom))/
        (pnorm(out$lambda[i-1]/denom)-pnorm(out$mp[i]/denom))
    }
  }
  return(pvals)
}

# Asymptotic spacing LAR p values list
spacing.pval.asymp.list= function(y,out,k,sigma=1) {
  if (length(out$lambda)==1) stop("The LAR path needs to be run for at least 2 steps.")
  pvals = numeric(k)
  for (i in 1:k) {
    v = out$Gamma[out$nk[i],]
    denom = sigma*sqrt(sum(v*v))
    if (i==1 && i<length(out$lambda)) {
      pvals[1] = (1-pnorm(out$lambda[i]/denom))/
        (1-pnorm(out$lambda[i+1]/denom))
    }
    else if (i<length(out$lambda)) {
      pvals[i] = (pnorm(out$lambda[i-1]/denom)-pnorm(out$lambda[i]/denom))/
        (pnorm(out$lambda[i-1]/denom)-pnorm(out$lambda[i+1]/denom))
    }
    else {
      pvals[i] = (pnorm(out$lambda[i-1]/denom)-pnorm(out$lambda[i]/denom))/
        (pnorm(out$lambda[i-1]/denom)-pnorm(0))
    }
  }
  return(pvals)
}

# Another approx to normal CDF
pnorm2 = function(x) {
  return(1/(1 + exp(-0.07056*x^3 - 1.5976*x)))
}
