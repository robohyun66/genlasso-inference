## Play around in 2D
workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
setwd(workingdir)

source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)


# form a 2D grid
m = 9
m0 = sqrt(m)
D = graph2D_Dmat(m)
makeDmat(m, type="trend.filtering", order=0)

# make image
img = matrix(0, ncol=m0, nrow=m0)
for(ii in 1:m0){
  for(jj in 1:m0){
    if(ii<jj+1){ img[ii,jj]=1}
  }
}

sigma=0.1
set.seed(0)
img0 = img + rnorm(m,0,sigma)
y0 = as.numeric(t(img0))
image(img0)

# fit fused lasso to it
numsteps=m-1
path = dualpathSvd2(y=y0,D=D,maxsteps=10,approx=F,verbose=T)
objects(path)
signif(y0,2)
path$action

# The first four breaks are as expected; at the borders of the 2d example
path$nk
dim(path$Gamma)

# sanity checks
min(path$Gamma %*% y0) # want this to be positive

# An example of a spike test 
step = 1
for(step in 1:11){
#  readline("Press enter for next:")
  v = D[path$action[step],]
  G = getGammat(path,y0,step,usage="dualpathSvd")
#  print(min(G%*%y0))
#  cat(which(G%*%y0<0), "out of", path$nk2, fill=TRUE)
  pval = pval.fl1d(y0,G,v,sigma,approxtype = "rob")
  print(pval)
}

# A segment test should take the fused segments and test them then!!
# There needs to be a scanner for connected components
nsim = 100
ngrain = 5
sigmalist = seq(from=0.1,to=0.5, length = ngrain)
pvals = matrix(NA, nrow = ngrain, ncol = nsim)
for(kk in 1:ngrain){
    print(kk)
    sigma = sigmalist[kk]
    print(sigma)
  for(isim in 1:nsim){
  cat('\r', isim, 'out of', nsim)
    # form a 2D grid
    m = 9
    m0 = sqrt(m)
    D = graph2D_Dmat(m)
    makeDmat(m, type="trend.filtering", order=0)

    # make image
    lev1=0
    lev2=1
    img = matrix(lev1, ncol=m0, nrow=m0)
    for(ii in 1:m0){
      for(jj in 1:m0){
        if(ii<jj+1){ img[ii,jj]=lev2}
      }
    }

  #  set.seed(0)
    img0 = img + rnorm(m,0,sigma)
    y0 = as.numeric(t(img0))

    path = dualpathSvd2(y=y0,D=D,maxsteps=10,approx=F,verbose=F)
    # insert scanner here for when connected components occur
    
    # instead, for now, condition!
    if (all(c(11,6,3,7) %in% path$action[1:4])){
      v = numeric(m)
      v[c(4,7,8)] = -5
      v[-c(4,7,8)] = 3
      v = v/15
      
      G = getGammat(path,y0,4,usage="dualpathSvd")
      pval = pval.fl1d(y0,G,v,sigma,approxtype = "rob")
      
      pvals[kk,isim] = pval
    }
  }
    cat(fill=T)
}

par(mfrow = c(2,3))
sapply(1:ngrain, function(jj){hist(pvals[jj,])})
dev.off()
