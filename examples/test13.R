# Comparing the linear contrast vector from the lars-transformed problem and 
# the segment test contrast from the genlasso (dual path) problem
# working directory should be [genlassoinf]/code
#setwd('~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code')
library(genlasso)
library(polypath)
source("funs.R")
source("pval.R")
source("dualPathSvd2.R")

# generate data
  set.seed(0)
  n = 100
  beta0 = rep(sample(1:10,5),each=n/5)
  sigma = .8
  z = beta0 + rnorm(n,sd=sigma)
  a = fusedlasso1d(z)

# Transform this into a lasso problem
  k = 0
  H = getH(n,k)
  H1 = H[,1:(k+1)]
  H2 = H[,(k+2):n]
  Pz = H1%*%solve(crossprod(H1),t(H1)%*%z)
  PH = H1%*% solve(crossprod(H1),t(H1)%*%H2)
  y = z - Pz
  X = H2 - PH
  rm(k)

  numsteps = 8
  out = lar(y,X,maxsteps=numsteps,verbose=TRUE)
  fits = as.numeric(Pz) + X%*%out$beta

# See difference in contrast
  pdf(file.path(outputdir,"diffcontrast.pdf"), width=9,height=9)
    par(mfrow=c(3,3))
    plot(z, main = "example data and signal(red)")
    lines(beta0, col = 'red')
    for(k in 1:numsteps){
      ## LARS
      Gk = out$Gamma[1:out$nk[k],]
      vk = out$Gamma[out$nk[k],]
      ### JUSTIN new code

      path = dualpathSvd2(z,maxsteps=numsteps,D=dual1d_Dmat(length(z)),approx=F)

      ## just checking if segment test contrast is being formed properly
      dvec0 = getdvec(path,z,k,usage="dualpathSvd",type="segment",matchstep=T)

      #if(!all.equal(dvec0 ,vk)) warning("segment test is violated!!!")
      print(dvec0/vk)
      ## plot all
      plot(dvec0,ylim = c(-2,2), main = paste("step",k), col = 'blue', type = 'o')
      abline(h = c(-1,1),lty=2,col='grey')
      lines(vk, col = 'red', type = 'o')
      legend("bottomleft", col = c("blue","red"), lty = c(1,1), pch = c(1,1),legend = c("dualgenlasso","lars"))
    }
  dev.off()


## See if p-values match
  path = dualpathSvd2(z,maxsteps=numsteps,D=dual1d_Dmat(length(z)),approx=T,verbose=TRUE)

  # Compute p values
  pvals2 = pvals3 = numeric(numsteps-1)
  for (k in 1:(numsteps-1)) {
    cat(k,"...\n")
    Gk = out$Gamma[1:out$nk[k],]
    ### SEGMENT
    vk = out$Gamma[out$nk[k],]
    dvec0 = getdvec(path,z,k,usage="dualpathSvd",type="segment",matchstep=T)

    ### JUSTIN
    Gjustin = path$Gamma[1:path$nk[k],]
    pvals2[k] = pval.fl1d(y,Gjustin,vk,sigma=sigma,threshold=T,approx=T,approxtype="rob")#Gv.pval(z,Gjustin,vk,sigma=sigma)
    pvals3[k] = pval.fl1d(y,Gjustin,dvec0,sigma=sigma,threshold=T,approx=T,approxtype="rob")#Gv.pval(z,Gjustin,dvec0,sigma=sigma)
      #pval.fl1d(y,Gjustin,vk,sigma=sigma,threshold=T,approx=T,approxtype="rob")#temp change
  }
