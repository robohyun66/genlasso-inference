## Make sure you're working from [dropboxfolder]/code
#workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
#setwd(workingdir)
source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)

### visualizing the fits with toy data
  pdf(file.path(outputdir, "bicrules.pdf"),width=14,height=7)
  viz = TRUE
  sigma = 4
  maxsteps = 100
  n = 100
  beta0 = c(rep(2,n/5),rep(16,n/5), rep(8,n/5),rep(10,n/5),rep(4,n/5))
  y0    = beta0 + rnorm(n,0,sigma)
  
  # obtain path
  f0 <- dualpathSvd2(y0, D=dual1d_Dmat(length(y0)), maxsteps=maxsteps,approx=T)
  
  # get bic and plot results
  consec = 3
  bic = getbic(y0,f0,sigma,maxsteps)
  dirs = c('min','forward','backward', 'logNrule')
  ind = c()
  par(mfrow = c(2,3))
  for(jj in 1:4){
    ind[jj] = which.rise(bic,type=dirs[jj],consec=consec,n)
    if(viz){
      plot(y0)
      lines(f0$beta[,ind[jj]],col='blue')
      abline(v = f0$pathobj$B[1:(ind[jj]-1)],lty=2,col='lightgrey')
      lines(beta0,col='red')
      title(paste(dirs[jj], " , ", (ind[jj]-1), " steps"))
      points(x = seq(from=20,to=80,by=20), y = rep(1,4), pch = 17, col = 'red')
    }
  }
  if(viz){
    plot(bic, ylim = c(min(bic),max(bic)+10))
    abline(v = ind, col = 'lightgrey', lty=2)
    text(x = ind, y = max(bic)+2, labels = dirs)
  }
  dev.off()


## obtain conditional power over noises
  consec = 2
  n = 50
  maxsteps = n
  nsim = 50
  ngrain = 5
  sigmalist = seq(from= .1, to = 8, length= ngrain)
  dirs = c('min','forward','backward', 'logNrule')
  conditpowermat = array(NA,dim=c(length(dirs),ngrain,n))
  powermat.oracle = matrix(NA, ncol = n, nrow = ngrain)
  pvals.segment = pvals.oracle = list()
  for(ii in 1:ngrain){
    cat('\n', ii, "out of", ngrain)
    sigma = sigmalist[ii]
    verdictmat = array(NA,c(length(dirs),nsim,n))
    verdictmat.oracle = array(NA,c(nsim,n))
    cat('\n')
    for(isim in 1:nsim){
       cat('\r', isim, "out of", nsim)

      # generate data + path
      beta0 = c(rep(2,n/5),rep(16,n/5), rep(8,n/5),rep(10,n/5),rep(4,n/5))
      y0    = beta0 + rnorm(n,0,sigma)
      f0 <- dualpathSvd2(y0, dual1d_Dmat(length(y0)), maxsteps,approx=T)

      ### conduct inference with forward-BIC stopping rule
      stoptimes = sapply(dirs, function(dir){ which.rise(getbic(y0,f0,sigma,maxsteps), consec,dir,n) - 1})
      for(ll in 1:length(stoptimes)){
        stoptime = stoptimes[ll]
        verdicts = pvals = c()
        for(test.step in 1:stoptime){
          pvals[test.step] <- pval.fl1d(y0, getGammat(f0,y0,stoptime,"dualpathSvd"), 
                                            getdvec(f0,y0,test.step,stoptime,type="segment",usage="dualpathSvd",matchstep=F), 
                                            sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
          verdicts[test.step]    <- (pvals[test.step]< (0.05/stoptime))
        }
        locs = f0$pathobj$B[1:stoptime]
        verdictmat[ll,isim,locs] = verdicts
      }
      
      ### conduct oracle inference
      breaks = 10*(0:5)
      verdict.oracle = p.oracles = c()
      for(kk in 2:5){
        # get p-value for oracle
        thisbreak = breaks[kk]
        dif = abs(mean(y0[(breaks[kk-1]+1):(breaks[kk])]) - mean(y0[(breaks[kk]+1):(breaks[kk+1])]))
        lvl = 0.05/(length(breaks)-2)
        n1 = n2 = 10
        z_crit = qnorm(1-lvl)*sigma*sqrt(1/n1 + 1/n2)
        verdict.oracle[kk] = dif > z_crit
        p.oracles[kk] = 1-pnorm(dif, mean=0, sd = sigma*sqrt(1/n1^2 + 1/n2^2))
        }
        locs = 10*(1:4)
        pmat.oracle[isim,locs] = p.oracles[-1]
        verdictmat.oracle[isim,locs] = verdict.oracle[-1]
    }
    for(ll in 1:length(dirs))   conditpowermat[ll,ii,] = apply(verdictmat[ll,,], 2, function(column){sum(sum(column, na.rm=T)/sum(!is.na(column)))})
    powermat.oracle[ii,] = apply(verdictmat.oracle, 2, function(column){sum(sum(column, na.rm=T)/sum(!is.na(column)))})
  }
  save(list = ls(), file = file.path(outputdir, "bicpowers-consec",consec,".Rdata")) 
  
  
  
  
  load(file = file.path(outputdir, "bicpowers.Rdata"))
  pdf(file.path(outputdir, "bicpowers.pdf"),width=14,height=7)
  par(mfrow=c(2,3))
  for(jj in 10*(1:4)){
    matplot(t(conditpowermat[,,jj]),type='l', col = 1:4, lty=1)
    lines(powermat.oracle[,10]~sigmalist,type='l', ylim = c(0,1),col='black', lwd=2)
    legend('topright', col = 1:4, lty = rep(1,4), legend = dirs)
  }
dev.off()
