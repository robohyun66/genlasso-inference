## Make sure you're working from [dropboxfolder]/code
#workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
#setwd(workingdir)
source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)
require(ggplot2)
require(plyr)
require(grid)
source("http://peterhaschke.com/Code/multiplot.R")
 
### Up-then-down signal Example ######

  # Generate stuff
  lev1  = 0 
  sigma = 1
  nsim = 10000
  
  # don't run unless necessary, find Rdata file first
  # load(file=file.path(outputdir,"hybridsegmentmadecut-uncond.Rdata")))
    lev2 = lev1 + 2
    p1spike = p1segment = p21spike = p21segment = rep(NA,nsim)

  # calculate CONDITIONAL power after one or two steps
    numsteps = 2
    ii = 0
    kk = 0
    jj = 0
    while(kk <= nsim & jj <= nsim){
      ii = ii + 1
      # generate data and obtain path + polyhedron
      y0 <- alternjump.y(lev1 = lev1, lev2 = lev2, sigma = sigma)
      beta0 <- alternjump.y(returnbeta=T, lev1=lev1, lev2=lev2, sigma = sigma)
      f0 <- dualpathSvd2(y0, dual1d_Dmat(length(y0)), maxsteps = numsteps,verbose=FALSE,approx=TRUE)
      
      if(f0$pathobj$B[1] %in% c(20,40)){
        # form polyhedron and contrasts
        G               <- getGammat(obj=f0,y=y0,k=1, usage = "dualpathSvd")
        d1spike         <- getdvec(obj=f0,y=y0,k=1,type="spike",usage="dualpathSvd", matchstep=T)
        d1segment       <- getdvec(obj=f0,y=y0,k=1,type="segment",usage="dualpathSvd", matchstep=T)
          
        #store things      
        p1spike[kk]              <- pval.fl1d(y0,G,d1spike,sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
        p1segment[kk]       <- pval.fl1d(y0,G,d1segment,sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
        kk=kk+1
      }

     if(f0$pathobj$B[1:2] == c(20,40) | f0$pathobj$B[1:2] == c(40,20) ){
        # form polyhedron and contrasts
        G2              <- getGammat(obj=f0,y=y0,k=numsteps,usage="dualpathSvd")
        d1spike         <- getdvec(obj=f0,y=y0,k=1,type="spike",usage="dualpathSvd",matchstep=T)
        d1segment       <- getdvec(obj=f0,y=y0,k=1,klater=2,type="segment",usage="dualpathSvd",matchstep=F)
     
        #store things
        p21spike[kk]         <- pval.fl1d(y0,G2,d1spike,  sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
        p21segment[kk]       <- pval.fl1d(y0,G2,d1segment,sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)      
        jj=jj+1
      }
      cat('\r', c(ii,kk,kk-jj))
    }
    propcorrect.step1 = kk/ii
    propcorrect.step2 = jj/ii
    save(list=c("lev1","lev2","sigma","p1spike", "p21spike", "p1segment", "p21segment", "nsim", "ii","jj","kk","propcorrect.step1", "propcorrect.step2"), file=file.path(outputdir,"updown-example.Rdata"))



  # plot it
    load(file=file.path(outputdir,"updown-example.Rdata"))
    pdf("output/updown-example.pdf", width=16,height= 4)
      cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")[c(2,3,8,4,6)] # http://www.cookbook-r.com/Graphs/Colors_%28ggplot2%29/
#      # Left
#      y0 <- alternjump.y(lev1 = lev1, lev2 = lev2)
#      beta0 <- alternjump.y(returnbeta=T, lev1=lev1, lev2=lev2)
#      plot(y0, xlab = "coordinate", ylab = "y", axes=F, main = "signal")
#      axis(1); axis(2)
#      lines(beta0, col = 'red', lwd=2)

      # Top left: the contrasts at step 1 being tested
      set.seed(1)
      sigma = 1
      y0    = alternjump.y(returnbeta=F,lev1=0,lev2=2,sigma=sigma,n=60)
      beta0 = alternjump.y(returnbeta=T,lev1=0,lev2=2,sigma=sigma,n=60)
      x0 = c(1:60)
      dt = data.frame(x0, y0, beta0)
      a1 <- ggplot(dt, aes(x=x0, y=y0, colour="x"))
      a1 <- a1 + geom_point()
      line.data = data.frame(x = x0, y = beta0, z = "signal")    
      a1 <- a1 + geom_line(aes(x=x0, y = beta0, color = "signal"), size = 1.2, data = line.data) +
                 scale_color_manual(name = "", values = c("x"="black", "signal"="red") ) + scale_linetype_manual(values = c("dashed","dashed"),  guide = guide_legend(override.aes = list(linetype = c("blank", "solid"), shape = c(1, NA))))
      a1 <- a1 + xlab("Coordinates") + ylab("y")
      a1 <- a1 + scale_x_continuous(limits = c(0, 70))
      a1 <- a1 + ggtitle("Data and Underlying Signal") + 
                 theme(plot.title = element_text(lineheight=.8, face="bold"))
      a1 <- a1 + theme(plot.margin = unit(c(1,2,0,0), "cm"))  
      a1 <- a1 + scale_fill_identity(guide = "legend")
      a1 <- a1 + geom_text(aes(x=c(45),y=c(2), label = "delta==2"),parse = T,size=3,fontface = 'plain')
      # Top Right: the contrasts at step 2 being tested                
      set.seed(0)
      sigma = 1
      y0    = alternjump.y(returnbeta=F,lev1=0,lev2=2,sigma=sigma,n=60)
      beta0 = alternjump.y(returnbeta=T,lev1=0,lev2=2,sigma=sigma,n=60)
      x0 = 1:60
      x0.spike = c(rep(NA,19),.3*c(-1,+1)-2, rep(NA,39))
      x0.segment = c(.3*rep(-0.7,20)-2 , .3*rep(+0.7,20)-2 , rep(NA,20))
            
      labs = expression(delta==0,delta==1,delta==2)
      x0 = c(1:60)
      dt = data.frame(y0, beta0, x0, x0.spike, x0.segment)
      a2 <- ggplot(dt, aes(x=x0, y=y0))
      a2 <- a2 + geom_point()
      line.data = data.frame(x = x0, y = beta0, z = "signal")    
      seg.data = data.frame(x = x0, y = x0.segment, z = "segment")
      spike.data = data.frame(x = x0, y = x0.spike, z = "spike")
      a2 <- a2 + geom_line(aes(x=x0, y = beta0, color = "signal"), size=1.2, data = spike.data) + 
                 geom_point(aes(x=x0, y = x0.segment, color = "segment"), size=3, data = seg.data) +
                 geom_point(aes(x=x0, y = x0.spike, color = "spike"), size=3, data = spike.data)           
      a2 <- a2 + scale_y_continuous(breaks=c(0:5),labels=c(0:5))                 
      a2 <- a2 + scale_x_continuous(limits = c(0, 70))
      a2 <- a2 + xlab("Coordinates") + ylab("y")
      a2 <- a2 + geom_text(aes(x=c(45),y=c(2), label = "delta==2"),parse = T,size=3,fontface = 'plain')

      a2 <- a2 + scale_colour_manual(name = "",
                                     values=c("signal"="red","x" ="black", "segment"="green", "spike" = "blue"))
      a2 <- a2 + theme(plot.margin = unit(c(1,2,0,0), "cm"))  
      ## todo: make some legends
      a2 <- a2 + ggtitle("Contrasts used by \n Spike and Segment Test")  + 
                 theme(plot.title = element_text(lineheight=.8, face="bold"))  
               
                      
      # bottom left
      my.data <- as.data.frame(rbind(cbind(p1spike,1),
                                     cbind(p21spike,2)))
      names(my.data) = c("V1","V2")
      colors <- cbPalette[1:2]
      labs <- expression(step~1, step~2)
      
      my.data$V2=as.factor(my.data$V2)
        
      res <- dlply(my.data, .(V2), function(x) density(x$V1))
      dd <- ldply(res, function(z){
                          data.frame(Values = z[["x"]], 
                                     V1_density = z[["y"]])
      })
      
      poffset.x = .2 # adapt 0.1 as needed
      dd$Values = dd$Values + rep(c(0,1)*poffset.x,each=512)
      dd$offest=-(as.numeric(dd$V2)-1)*.2# adapt the 1 value as you need
      dd$V1_density_offest = dd$V1_density+dd$offest
        

      a3 <- ggplot(dd) 
      a3 <- a3 + geom_line( aes(Values, V1_density_offest, color=V2), size = 1.5)
      a3 <- a3 + geom_ribbon(aes(Values, ymin=offest,ymax=V1_density_offest, fill=V2),alpha=0.3)
      a3 <- a3 + scale_color_manual(values=colors,guide=FALSE)
      a3 <- a3 + scale_x_continuous(breaks=NULL) 
      a3 <- a3 + scale_y_continuous(breaks=c(0:5),labels=c(0:5))
      a3 <- a3 + xlab("p-values") + ylab("Fitted Density")
      a3 <- a3 + scale_fill_manual(name="Conditioning Steps",
                                 values=colors,
                                 labels=labs)
      a3 <- a3 + ggtitle("Spike Test p-value Distribution \n Correct Location") + 
               theme(plot.title = element_text(lineheight=.8, face="bold"))

      # bottom right
      my.data <- as.data.frame(rbind(cbind(p1segment,1),
                                     cbind(p21segment,2)))
      names(my.data) = c("V1","V2")
      colors <- cbPalette[1:2]
      labs <- expression(step~1, step~2)
      
      my.data$V2=as.factor(my.data$V2)
        
      res <- dlply(my.data, .(V2), function(x) density(x$V1))
      dd <- ldply(res, function(z){
                          data.frame(Values = z[["x"]], 
                                     V1_density = z[["y"]])
      })
      
      poffset.x = .2 # adapt 0.1 as needed
      dd$Values = dd$Values + rep(c(0,1)*poffset.x,each=512)
      dd$offest=-(as.numeric(dd$V2)-1)*.2# adapt the 1 value as you need
      dd$V1_density_offest = dd$V1_density+dd$offest
        

      a4 <- ggplot(dd) 
      a4 <- a4 + geom_line( aes(Values, V1_density_offest, color=V2), size = 1.5)
      a4 <- a4 + geom_ribbon(aes(Values, ymin=offest,ymax=V1_density_offest, fill=V2),alpha=0.3)
      a4 <- a4 + scale_color_manual(values=colors,guide=FALSE)
      a4 <- a4 + scale_x_continuous(breaks=NULL) 
      a4 <- a4 + scale_y_continuous(breaks=c(0:5),labels=c(0:5))
      a4 <- a4 + xlab("p-values") + ylab("Fitted Density")
      a4 <- a4 + scale_fill_manual(name="Conditioning Steps",
                                 values=colors,
                                 labels=labs)
      a4 <- a4 + ggtitle("Spike Test p-value Distribution \n Correct Location") + 
               theme(plot.title = element_text(lineheight=.8, face="bold"))

      multiplot(a2, a3, a4, layout = matrix(c(1,2,3),nrow=1,byrow=T), cols=3) 
      
    dev.off()



  
### Seeing how frequently FL catches the correct jump location in one-jump example ###
  # Generate Data
  lev1=0
  n=60
  sigma=1
  nsim=10000
  gaps = 0:2
  locations = matrix(ncol = length(gaps), nrow = nsim)
  for(gapsize in gaps){
    cat("\r", gapsize, "of", gaps )
    lev2 = lev1 + gapsize
    cat("\n","a")
    for(jj in 1:nsim){
      cat("\r", jj, "of", nsim )
      y0 = onejump.y(returnbeta=F,lev1=lev1,lev2=lev2,sigma=sigma,n=n)
      f = dualpathSvd2(y0,D=dual1d_Dmat(length(y0)),maxsteps=1)
      locations[jj,gapsize+1] = f$pathobj$B[1]
    }
    cat("\n","\n")
  }
  cat("\n","\n")
  save(file=file.path(outputdir, "onejump-freq.Rdata"), list = c("locations","gaps","nsim","sigma","n","lev1"))

  # Plot it
  load(file=file.path(outputdir, "onejump-freq.Rdata"))
  cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")[c(2,3,8,4,6)]
  densities = list()
  for(jj in 1:length(gaps)){
    densities[[jj]] = density(locations[,jj])
  }
  plot(densities[[length(gaps)]],col='white')
  for(jj in 1:length(gaps)){
    lines(densities[[jj]],col=cbPalette[jj])
  }

  # calculate what we want
  load(file=file.path(outputdir, "onejump-freq.Rdata"))
  sum(locations[,1]==30)/nsim
  sum(locations[,2]==30)/nsim
  sum(locations[,3]==30)/nsim



