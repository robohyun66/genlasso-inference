## Make sure you're working from [dropboxfolder]/code
  workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
  setwd(workingdir)
  source("settings.R")
  source('selectinf/selectiveInference/R/funs.inf.R')
  source('funs.R')
  source('examples/testfuns.R')
  #source('pval.R')
  source('dualPathSvd2.R')
  library(genlasso)
  library(polypath)
  library(igraph)
  verbose = F

# average winter temperature
  temprmat = read.table("examples/data/temperature.txt",header=TRUE)
  temprmat[,1] = levels(temprmat[,1])[as.numeric(temprmat[,1])]
  tempr = temprmat[,"Avg.F"]

# republican votes percentage
  votes = read.table("examples/data/votes.csv", header=TRUE,sep=",")
  head(votes)
  votes[,1] = levels(votes[,1])[as.numeric(votes[,1])]
  repvotes = votes[,"ROMNEY.R."]

# median per-capita income 
  income = read.table("examples/data/percapita.csv", header=F, sep = ",")
  percapita = income[,2]
  income[,1] = levels(income[,1])[as.numeric(income[,1])]
  #percapita = levels(income[,2])[as.numeric(income[,2])]

# checking that states match up
  cbind(temprmat[,1],votes[,1], income[,1] )

# latitude and longitude
  latlong = read.table("examples/data/latlong.csv", sep=',',header=TRUE)
  head(latlong)
  states = as.character(latlong[,1])
  
# save all relevant data  
  save(tempr, repvotes, percapita, latlong, states, file=file.path(outputdir,"2012election-bystate.Rdata"))
