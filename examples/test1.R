library(lars)
library(polypath)
source("pval.R")

set.seed(0)
n = 20
p = 10
X = matrix(rnorm(n*p),n,p)
s = 2
beta = c(rep(2,s),rep(0,p-s))
y = X%*%beta + rnorm(n)

# LAR
out1 = lars(X,y,type="lar",normalize=FALSE,intercept=FALSE)
out2 = lar(y,X,verbose=TRUE)

# Some checks against the lars package
max(abs(out1$lambda-out2$lambda))
max(abs(t(out1$beta[-nrow(out1$beta),])-out2$beta))

# Some checks for the Gamma matrix
lam2 = out2$Gamma[out2$nk,] %*% y
max(abs(out2$lambda-lam2))
min(out2$Gamma %*% y)
nrow(out2$Gamma)

# Compute p values for the LAR models, 3 different ways
k = 8
pvals1 = lar.pval.list(y,out2,k)
pvals2 = spacing.pval.list(y,out2,k)
pvals3 = spacing.pval.asymp.list(y,out2,k)

# Lasso
out3 = lars(X,y,type="lasso",normalize=FALSE,intercept=FALSE)
out4 = lasso(y,X,verbose=TRUE)

# Some checks against the lars package
max(abs(out3$lambda-out4$lambda))
max(abs(t(out3$beta[-nrow(out3$beta),])-out4$beta))

# Some checks for the Gamma matrix
lam2 = out4$Gamma[out4$nk,] %*% y
max(abs(out4$lambda-lam2))
min(out4$Gamma %*% y)
nrow(out4$Gamma)
