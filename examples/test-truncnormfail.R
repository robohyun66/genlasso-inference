## Make sure you're working from [dropboxfolder]/code
#workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
#setwd(workingdir)
source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)


# Showing that small signal-to-noise ratio gives NaN p-values at signals
sigma=0.0001 # sigma = 2
beta0 = alternjump.y(returnbeta=T, lev1=1, lev2=3, sigma=sigma, n=60)
y0    = alternjump.y(returnbeta=F, lev1=1, lev2=3, sigma=sigma, n=60)
f0    = dualpathSvd2(y0, dual1d_Dmat(length(y0)), maxsteps,approx=T)

stoptimes = c(1,2,3,5,10)
for(ll in 1:length(stoptimes)){
  stoptime = stoptimes[ll]
  verdicts = pvals = c()
  for(test.step in 1:stoptime){
    G = getGammat(f0,y0,stoptime,"dualpathSvd")
    d = getdvec(f0,y0,test.step,stoptime,type="segment",usage="dualpathSvd",matchstep=F)
    cat('\n','test step:', test.step, 'and testing', (f0$pathobj$B)[test.step], ' while conditioning on step:', stoptime, '\n')
    print(pvals[test.step] <- pval.fl1d(y0, G, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE))
  }
}

# conclusion: NaNs probably mean very low p-value, so I might assign 0 to it.

plot(y0)
lines(beta0, col = 'red')
abline(v=f0$pathobj$B[1:3],col='grey')
