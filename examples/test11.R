## previously called test11.R
# working directory should be [genlassoinf]/code
# setwd('~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code')
source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
#source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)
library(gap) # allows use of uniform qqplot

# 1. Verifying that null p-values are uniform.
  n = 60
  sigma = .5
  beta0 = rep(5,n)
  y <- beta0 + rnorm(n,sd=sigma)
  nsim = 1000
  pdf(file.path(outputdir,"nullpvalssegment.pdf"),width=10,height=6)
  par(mfrow=c(2,3))
    plot(y, main = "", ylab = "y", xlab = "x", ylim = c(-1,7))
    lines(beta0, col = 'red')  
    k0 = 30
   # p1spike <- replicate(nsim, nullp(k0 = k0, type = "spike", approx=T,approxtype = "gsell",threshold=T))
   # p1segment <- replicate(nsim, nullp(k0 = k0, type = "segment", approx=T,approxtype = "gsell",threshold=T))
    boxplot(list(p1spike, p1segment), main = paste(k0,"th gap"))
    hist(p1spike)
    hist(p1segment)
    qqunif(p1spike, main = "spike",logscale=FALSE)
    qqunif(p1segment, main = "segment",logscale=FALSE)
  dev.off()
  
    
  
# 2. one-jump case:  what are some basic results of segment&spike test results (say, testing along the path)?
  nsim = 100
  nstep = 5
  pspikelist = psegmentlist = list()
  for(step in 1:nstep){
    pspike = psegment = c()
    cat(step," ")
    for(i.sim in 1:nsim){
      sigma = .5
      y0 <- onejump.y(returnbeta=F,sigma=sigma)
      f0 <- fl1d(y0,nstep)
      d0_spike <- getdvec(f0, y0, k=step, type = 'spike', matchstep=T)
      d0_segment <- getdvec(f0, y0, k=step, type = 'segment', matchstep=T)
      G0 <- getGammat(f0,y0,k=step,usage="fl1d")
      psegment[i.sim] <- pval.fl1d(y=y0,G=G0,dik=d0_segment,sigma=sigma,approx=T, approxtype = "rob", threshold=T)
      pspike[i.sim] <- pval.fl1d(y=y0,G=G0,dik=d0_spike,sigma=sigma,approx=T, approxtype = "rob",threshold=T)
    }
    psegmentlist[[step]] <- psegment
    pspikelist[[step]]   <- pspike
  }
  save(list=c("psegmentlist", "pspikelist"),file=file.path(outputdir,"onejumpsegmentstuff.Rdata"))

# load and plot some stuff
  load(file=file.path(outputdir,"onejumpsegmentstuff.Rdata"))
  pdf(file.path(outputdir,"onejumpsegment.pdf"),width=8,height=8)
    par(mfrow = c(3,2))
    plot(onejump.y(returnbeta=F,sigma=sigma),ylab="y", main = "data example")
    lines(onejump.y(returnbeta=T,sigma=sigma),col='red')
    for(ii in 1:5){
      boxplot(cbind(psegmentlist[[ii]],pspikelist[[ii]]),axes=F,main = paste("step: ",ii))
      axis(1, labels=c("segment","spike"), at = c(1,2)); axis(2)
    }
  dev.off()
