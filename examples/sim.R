workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code/justin'
# change to your working directory TODO: make command-line-runnable structure.
setwd(workingdir)
source('~/helpers.R') # generic helpers
source('functions.R')
source('../pval.R')
sourceDir('genlassoRcode')
library(genlasso)

## simulation settings
m = 10 ## number of data points
set.seed(1)
y = rnorm(m,10,2)
#y = c(rep(10, (m/6)), rep(5, (m/6)),rep(10, (m/6)),rep(23, (m/6)),rep(5, (m/6)),rep(13, (m/6))) + rnorm(m,0,.5)
#y = c(rep(10, (m/4)), rep(5, (m/4)),rep(10, (m/4)),rep(23, (m/4)))
y = c(rep(5, (m/2)),rep(23, (m/2)))+ rnorm(m,0,.5)
D = dual1d_Dmat(m)


## run two methods
fl1d_mine     = fl1d(y)
fl1d_compare  = fusedlasso1d(y)
fl1d_compare2 = dualpathSvd2(y, D)

## compare the dual path
fl1d_mine$u
fl1d_compare$u
fl1d_compare2$u

## plotting the dual path
plot.fl1d(fl1d_mine)

## getting primal
getprimalmat(fl1d_mine$u,y,D)
fl1d_compare$beta

## get pvalues
n = length(y)
pvals = rep(NA,n-1)
for(k in 1:(n-1) ){
  Gammatk = getGammat(fl1d_mine,y,k)
  dik     = getdvec(fl1d_mine,y,k)
  pvals[k] = lar.pval(y,Gammatk,dik)
}
primalmat = getprimalmat(fl1d_mine$u,y)


## plot pvalues and path
plotfilename = paste0("toy2.pdf")
pdf(plotfilename, width=7,height=14, onefile = T)
for(k in 1:5){
 par(mfrow = c(2,1))
 # plot primal solution at step k
    plot(y, xlim = c(-1,n+1), ylim = c(min(y)-2, max(y)+2))
    for(kk in 1:k){
      jumpat = fl1d_mine$B[kk]
      text(x = jumpat, y = 14, kk, adj = c(1,1))
      abline(v = jumpat, lty = 3)
    }
    lines(primalmat[,(k+1)], col='red')
    
  # plot pvalues
     plot(pvals[1:k], xlim = c(0,10), ylim = c(0,1), col = 'red')
   text(x = 1:k, y = jitter(pvals[1:k]), signif(pvals[1:k],2), adj = c(0.2,-2))
   abline(v = 1:k, lty = 3)
}
dev.off()

