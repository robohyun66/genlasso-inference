## Make sure you're working from [dropboxfolder]/code
#workingdir = '~/../../media/shyun/Bridge/Dropbox/CMU/courses(CURRENT)/genlassoinf/code'
#setwd(workingdir)
source("settings.R")
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)

### Narrow Pulse Example

  # generate stuff 
  lev1 = 1
  lev2 = 10
  sigma = 1
  nsim = 10000
  pvals.spike1 =  pvals.spike2 = pvals.segment1 = pvals.segment2 =  c()
  # run experiment nsim times, store results
  for(i.sim in 1:nsim){
    cat("\r", i.sim, "of", nsim)
    beta0 = c(rep(lev1,30),rep(lev2,2),rep(lev1,30))
    n = length(beta0)
    y0 = beta0 + rnorm(n,0,sigma)
    path = dualpathSvd2(y0,dual1d_Dmat(n),maxsteps=2,approx=T)
    
    d.spike1   = getdvec(obj=path, y=y0, k=1, usage = "dualpathSvd", type="spike", matchstep=TRUE)
    d.segment1 = getdvec(obj=path, y=y0, k=1, usage = "dualpathSvd", type="segment", matchstep=TRUE)

    d.spike2   = getdvec(obj=path, y=y0, k=2, usage = "dualpathSvd", type="spike", matchstep=TRUE)
    d.segment2 = getdvec(obj=path, y=y0, k=2, usage = "dualpathSvd", type="segment", matchstep=TRUE)

    G1 = path$Gammat[1:path$nk[1],]
    G2 = path$Gammat[1:path$nk[2],]
    
    pvals.spike1[i.sim]   = pval.fl1d(y=y0, G=G1, dik=d.spike1, sigma=sigma, approx=TRUE, threshold=TRUE, approxtype="rob")
    pvals.segment1[i.sim] = pval.fl1d(y=y0, G=G1, dik=d.segment1, sigma=sigma, approx=TRUE, threshold=TRUE, approxtype="rob")
    pvals.spike2[i.sim]   = pval.fl1d(y=y0, G=G2, dik=d.spike2, sigma=sigma, approx=TRUE, threshold=TRUE, approxtype="rob")
    pvals.segment2[i.sim] = pval.fl1d(y=y0, G=G2, dik=d.segment2, sigma=sigma, approx=TRUE, threshold=TRUE, approxtype="rob")
  }
  save(list=c("lev1","lev2","sigma","pvals.spike1", "pvals.spike2", "pvals.segment1", "pvals.segment2", "nsim"), 
       file=file.path(outputdir,"narrowpulse.Rdata"))

  
  # plot it
  load(file.path(outputdir, "narrowpulse.Rdata"))
  pdf("output/narrowpulse.pdf", width=10,height=4)
    par(mfrow = c(1,3))
    # Left
    beta0 = c(rep(lev1,30),rep(lev2,2),rep(lev1,30))
    n = length(beta0)
    y0 = beta0 + rnorm(n,0,sigma)
    plot(y0, xlab = "coordinate", ylab = "y")
    lines(beta0, col = 'red', lwd=2)
    col1 = rgb(0,.5,1,0.2)
    col2 = rgb(1,0,0,0.4)
    # Middle
    hist(pvals.spike1, col = col1, freq = F, xlab = "p-values", main = "First Step")  
    hist(pvals.segment1, col = col2, freq = F, xlab = "p-values", add=T)
    legend("topright", fill = c(col1,col2), legend = c("spike", "segment"))
    # Right
    hist(pvals.segment2, col = col2, freq = F, xlab = "p-values", main = "Second Step")
    hist(pvals.spike2, col = col1, freq = F, xlab = "p-values", add=T)  
    legend("topright", fill = c(col1,col2), legend = c("spike", "segment"))
  dev.off()
  
  
## Originally in powers-bysignalsize
  pdf(file.path(outputdir, "narrowpulse-power-bywidth.pdf"), width = 10, height = 4)
   #third row:
    #left
    par(mfrow = c(1,3))
    lev1=3
    lev2=10
    beta0 = c(rep(lev1,30),rep(lev2,2),rep(lev1,30))
    n = length(beta0)
    sigma=1
    set.seed(0)
    y0 = beta0 + rnorm(n,0,sigma)
    plot(y0, axes=F, cex=.5, ylab = "y", xlab = "coordinate")
    lines(beta0, col = 'red', lwd=2)
    axis(1);axis(2)
    
    #middle: plot unconditional powers at step 2
    load(file=file.path(outputdir,"uncondspike-bywidth.Rdata"))
    plot(x =  widths, y = power.spike1, ylim = c(0,1), ylab = "unconditional power", xlab = "plateau width", main = "uncond. power by hump width, spike test", type = 'o', cex=.5, axes=F)
    axis(1);axis(2)
    lines(x =  widths, y = power.spike2, col = 'black', type='o', cex=.5,lty=2)
    legend("topleft", col = c("black","black"), pch = c(1,1), lty = c(1,2), cex = c(1,1),legend = c("1st step","2nd step"))

    # right: plot unconditional powers at step 2
    plot(x =  widths, y = power.segment1, ylim = c(0,1), ylab = "unconditional power", col = 'blue', xlab = "plateau width", main = "uncond. power by hump width, at second step", type = 'o', cex=.5,axes=F)
    axis(1);axis(2)
    lines(x =  widths, y = power.segment2, col = 'blue', type='o', cex=.5, lty=2)
    legend("topleft", col = c("blue","blue"), pch = c(1,1), lty = c(1,2), cex = c(1,1),legend = c("1st step","2nd step"))
  dev.off()
  
# Ideas: We can see at small signal strengths, both are close and equally poor. 
# So four figures: from the left
# 1. Figure with data and signal, with delta1 and delta2 indicating varying signal size and plateau size
# 2. Powers for spike test at first and second step (along the way, but optionally looking back too!)
# 3. Powers for segment test at first and second step  (along the way, but optionally looking back too!)
# 4 and 5. Power curves for varying delta1 and delta2 respectively!


