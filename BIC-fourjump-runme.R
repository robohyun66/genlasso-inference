# either hard code these
#codedir = #path/to/your/code
#outputdir = #relative/path/from/codedir/to/where/you/want/your/Rdatafile

# or read them from your command line input
args = commandArgs(trailingOnly = TRUE)
outputdir = args[1]

# some helpers and functions
source('helpers.R') # generic helpers
source('funs.R')
source('examples/testfuns.R')
source('pval.R')
source('dualPathSvd2.R')
library(genlasso)
library(polypath)

# main body of code
source("BIC-fourjump-body.R")
