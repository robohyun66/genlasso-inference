## Why it exists: to be sourced in by BIC-onejump-runme.R
## What it does: Obtains pvalues and test results at all locations/replicates, saves as Rdata file

  consec = 2
  nlist = 2*c(10,40,90)
  sigmamaxlist = 3*(1:3)
  nsim = 100000
  ngrain = 20
  lev1 = 0
  lev2 = 1
  nchunks = 100
  nsimchunk = nsim/nchunks
  obj.list1 = c("stoptimes.bic", "stoptimes.bic2", "stoptimes.ebic","stoptimes.sbic", "stoptimes.aic", 
                 "pvals.bic", "pvals.bic.naive", "verdicts.bic", "verdicts.bic.naive",
                 "pvals.bic2", "pvals.bic2.naive", "verdicts.bic2", "verdicts.bic2.naive", 
                 "pvals.ebic", "pvals.ebic.naive", "verdicts.ebic", "verdicts.ebic.naive",  
                 "pvals.sbic", "pvals.sbic.naive", "verdicts.sbic", "verdicts.sbic.naive",  
                 "pvals.aic", "pvals.aic.naive", "verdicts.aic", "verdicts.aic.naive", 
                 "pvals.fixed1", "pvals.fixed2","verdicts.fixed1", "verdicts.fixed2",
                 "pvals.oracle","verdicts.oracle",
                 "sigma","nsimchunk","nchunks","nsim","ngrain","lev1","lev2","n", "sigmalist")


  for(kk in 1:3){
    n = nlist[kk]
    maxsteps = n/2  # this is because.. computation time sucks
    cat('\n', n, "out of", nlist, "sample size")
    for(chunk.i in 1:nchunks){
      cat('\n', chunk.i, "out of", nchunks, "chunks")
      # things to store
        stoptimes.bic = stoptimes.bic2 = stoptimes.ebic = stoptimes.sbic = stoptimes.aic = array(NA, c(ngrain,nsimchunk))
        pvals.bic = pvals.bic.naive = 
        pvals.bic2 = pvals.bic2.naive = 
        pvals.ebic = pvals.ebic.naive = 
        pvals.sbic = pvals.sbic.naive = 
        verdicts.bic = verdicts.bic.naive = 
        verdicts.bic2 = verdicts.bic2.naive = 
        verdicts.ebic = verdicts.ebic.naive = 
        verdicts.sbic = verdicts.sbic.naive = 
        pvals.aic = pvals.aic.naive = 
        verdicts.aic = verdicts.aic.naive = array(NA,c(ngrain, nsimchunk, n))
        pvals.fixed1 = pvals.fixed2 = verdicts.fixed1 = verdicts.fixed2 = array(NA,c(ngrain, nsimchunk, n))
        pvals.oracle = verdicts.oracle = array(NA,c(ngrain,nsimchunk))
        gc()
        cat('\n')
      sigmalist = seq(from= 0.1, to = sigmamaxlist[kk], length= ngrain)
      for(ii in 1:ngrain){ #foreach(ii = 1:ngrain) %dopar% {#
        cat('\r', ii, "out of", ngrain, "grain")
        sigma = sigmalist[ii]
#        cat('\n')
        for(isim in 1:nsimchunk){
#          cat('\r', isim, "out of", nsimchunk)

          # generate data + path
          beta0 = onejump.y(returnbeta=T, lev1=lev1, lev2=lev2, sigma=sigma, n=n)  # this could change
          y0    = onejump.y(returnbeta=F, lev1=lev1, lev2=lev2, sigma=sigma, n=n)
          f0    = dualpathSvd2(y0, dual1d_Dmat(length(y0)), maxsteps,approx=T)

          ### 1. collect information
          # bic
          stoptime.bic = which.rise(getbic(y0,f0,sigma,maxsteps),consec, n) - 1 # internally defining the `stoptime' to be the step of the algorithm where you stop. the stoptime to be plotted is this+1.
          stoptime.bic = pmin(stoptime.bic,n-consec-1)
          if(stoptime.bic > 0){ # when stoptime is zero, no test is conducted.  
          locs.bic = f0$pathobj$B[1:stoptime.bic]
          Gobj    = getGammat(f0,y0,stoptime.bic+consec,"dualpathSvd",'bic',sigma,consec,maxsteps)
          G       = Gobj$Gammat
          u       = Gobj$u
          G.naive = getGammat(f0,y0,stoptime.bic+consec,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:stoptime.bic){
            d = getdvec(f0,y0,test.step,stoptime.bic,type="segment",usage="dualpathSvd",matchstep=F)
            loc = locs.bic[test.step]
            pvals.bic[ii, isim, loc]       <- pval.fl1d(y0, G,       d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE, u)
            pvals.bic.naive[ii, isim, loc] <- pval.fl1d(y0, G.naive, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
            verdicts.bic[ii, isim, loc]       <- (pvals.bic[ii, isim, loc] < (0.05/stoptime.bic))
            verdicts.bic.naive[ii, isim, loc] <- (pvals.bic.naive[ii, isim, loc] < (0.05/stoptime.bic))
          }
          }
          
          # bic2
          stoptime.bic2 = which.rise(getbic(y0,f0,sigma,maxsteps,fac=2),consec, n) - 1 # internally defining the `stoptime' to be the step of the algorithm where you stop. the stoptime to be plotted is this+1.
          stoptime.bic2 = pmin(stoptime.bic2,n-consec-1)
          if(stoptime.bic2 > 0){ # when stoptime is zero, no test is conducted.  
          locs.bic2 = f0$pathobj$B[1:stoptime.bic2]
          Gobj    = getGammat(f0,y0,stoptime.bic2+consec,"dualpathSvd",'bic',sigma,consec,maxsteps)
          G       = Gobj$Gammat
          u       = Gobj$u
          G.naive = getGammat(f0,y0,stoptime.bic2+consec,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:stoptime.bic2){
            d = getdvec(f0,y0,test.step,stoptime.bic2,type="segment",usage="dualpathSvd",matchstep=F)
            loc = locs.bic2[test.step]
            pvals.bic2[ii, isim, loc]       <- pval.fl1d(y0, G,       d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE, u)
            pvals.bic2.naive[ii, isim, loc] <- pval.fl1d(y0, G.naive, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
            verdicts.bic2[ii, isim, loc]       <- (pvals.bic2[ii, isim, loc] < (0.05/stoptime.bic2))
            verdicts.bic2.naive[ii, isim, loc] <- (pvals.bic2.naive[ii, isim, loc] < (0.05/stoptime.bic2))
          }
          }
          
          
          # sbic: strengthened bic
          stoptime.sbic = which.rise(getbic(y0,f0,sigma,maxsteps,strength=1.01),consec, n) - 1 # internally defining the `stoptime' to be the step of the algorithm where you stop. the stoptime to be plotted is this+1.
          stoptime.sbic = pmin(stoptime.sbic,n-consec-1)
          if(stoptime.sbic > 0){ # when stoptime is zero, no test is conducted.  
          locs.sbic = f0$pathobj$B[1:stoptime.sbic]
          Gobj    = getGammat(f0,y0,stoptime.sbic+consec,"dualpathSvd",'bic',sigma,consec,maxsteps)
          G       = Gobj$Gammat
          u       = Gobj$u
          G.naive = getGammat(f0,y0,stoptime.sbic+consec,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:stoptime.sbic){
            d = getdvec(f0,y0,test.step,stoptime.sbic,type="segment",usage="dualpathSvd",matchstep=F)
            loc = locs.sbic[test.step]
            pvals.sbic[ii, isim, loc]       <- pval.fl1d(y0, G,       d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE, u)
            pvals.sbic.naive[ii, isim, loc] <- pval.fl1d(y0, G.naive, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
            verdicts.sbic[ii, isim, loc]       <- (pvals.sbic[ii, isim, loc] < (0.05/stoptime.sbic))
            verdicts.sbic.naive[ii, isim, loc] <- (pvals.sbic.naive[ii, isim, loc] < (0.05/stoptime.sbic))
          }
          }
          
           
          # ebic: extended bic
          stoptime.ebic = which.rise(getebic(y0,f0,sigma,maxsteps, fac=.5),consec, n) - 1 # internally defining the `stoptime' to be the step of the algorithm where you stop. the stoptime to be plotted is this+1.
          stoptime.ebic = pmin(stoptime.ebic,n-consec-1)
          if(stoptime.ebic > 0){ # when stoptime is zero, no test is conducted.  
          locs.ebic = f0$pathobj$B[1:stoptime.ebic]
          Gobj    = getGammat(f0,y0,stoptime.ebic+consec,"dualpathSvd",'bic',sigma,consec,maxsteps)
          G       = Gobj$Gammat
          u       = Gobj$u
          G.naive = getGammat(f0,y0,stoptime.ebic+consec,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:stoptime.ebic){
            d = getdvec(f0,y0,test.step,stoptime.ebic,type="segment",usage="dualpathSvd",matchstep=F)
            loc = locs.ebic[test.step]
            pvals.ebic[ii, isim, loc]       <- pval.fl1d(y0, G,       d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE, u)
            pvals.ebic.naive[ii, isim, loc] <- pval.fl1d(y0, G.naive, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
            verdicts.ebic[ii, isim, loc]       <- (pvals.ebic[ii, isim, loc] < (0.05/stoptime.ebic))
            verdicts.ebic.naive[ii, isim, loc] <- (pvals.ebic.naive[ii, isim, loc] < (0.05/stoptime.ebic))
          }
          }


          # aic
          stoptime.aic = which.rise(getaic(y0,f0,sigma,maxsteps),consec, n)-1
          stoptime.aic = pmin(stoptime.aic, n-consec-1)
          if(stoptime.aic > 0){ # when stoptime is zero, no test is conducted.  
            locs.aic = f0$pathobj$B[1:stoptime.aic]
            Gobj    = getGammat(f0,y0,stoptime.aic+consec,"dualpathSvd",'aic',sigma,consec,maxsteps)
            G       = Gobj$Gammat
            u       = Gobj$u
            G.naive = getGammat(f0,y0,stoptime.aic+consec,"dualpathSvd",maxsteps=maxsteps)
            for(test.step in 1:stoptime.aic){
              d = getdvec(f0,y0,test.step,stoptime.aic,type="segment",usage="dualpathSvd",matchstep=F)
              loc = locs.aic[test.step]
              pvals.aic[ii, isim, loc]       <- pval.fl1d(y0, G,       d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE,u)
              pvals.aic.naive[ii, isim, loc] <- pval.fl1d(y0, G.naive, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
              verdicts.aic[ii, isim, loc]       <- (pvals.aic[ii, isim, loc]< (0.05/stoptime.aic))
              verdicts.aic.naive[ii, isim, loc] <- (pvals.aic.naive[ii, isim, loc]< (0.05/stoptime.aic))
            }
          }

          # fixed stop times
          fixedstoptime = 1
          G.truth = getGammat(f0,y0,fixedstoptime,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:fixedstoptime){
             d = getdvec(f0,y0,test.step,fixedstoptime,type="segment",usage="dualpathSvd",matchstep=F)
             loc = f0$pathobj$B[test.step]
             pvals.fixed1[ii, isim, loc]       <- pval.fl1d(y0, G.truth, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
             verdicts.fixed1[ii, isim, loc]       <- (pvals.fixed1[ii, isim, loc] < (0.05/fixedstoptime))
          }
          fixedstoptime = 2
          G.truth = getGammat(f0,y0,fixedstoptime,"dualpathSvd",maxsteps=maxsteps)
          for(test.step in 1:fixedstoptime){
             d = getdvec(f0,y0,test.step,fixedstoptime,type="segment",usage="dualpathSvd",matchstep=F)
             loc = f0$pathobj$B[test.step]
             pvals.fixed2[ii, isim, loc]       <- pval.fl1d(y0, G.truth, d, sigma, approx=TRUE, approxtype = "rob", threshold=TRUE)
             verdicts.fixed2[ii, isim, loc]       <- (pvals.fixed2[ii, isim, loc] < (0.05/fixedstoptime))
          }

          # store stoptimes
          stoptimes.bic[ii,isim] = stoptime.bic
          stoptimes.bic2[ii,isim] = stoptime.bic2
          stoptimes.ebic[ii,isim] = stoptime.ebic
          stoptimes.sbic[ii,isim] = stoptime.sbic
          stoptimes.aic[ii,isim] = stoptime.aic
          
          # store oracle
          brk = n/2
          dif = abs(mean(y0[(brk+1):n]) - mean(y0[1:brk]))
          lvl = 0.05/1
          n1 = n2 = n/2
          z_crit = qnorm(1-lvl)*sigma*sqrt(1/n1 + 1/n2)
          verdicts.oracle[ii,isim] = dif > z_crit
          pvals.oracle[ii,isim]      = 1-pnorm(dif, mean=0, sd = sigma*sqrt(1/n1^2 + 1/n2^2))
        }
      }
    # save
    save(list=obj.list1, file=file.path(outputdir, paste0("bic-onejump-segmentsize-allbics", n/2, "-chunk" ,chunk.i,".Rdata")))
    }
  }
  
  
  
